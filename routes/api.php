<?php

use Illuminate\Contracts\Routing\Registrar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Tài khoản người sử dụng
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'user',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'UserController@index');
        $router->get('getPaginate', 'UserController@getPaginate');
        $router->post('create', 'UserController@create');
        $router->get('show/{id}', 'UserController@show');
        $router->post('update/{id}', 'UserController@update');
        $router->delete('remove/{id}', 'UserController@remove');
        $router->post('searchAll', 'UserController@searchAll');
    });
});

// Classroom
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'classroom',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('getList', 'ClassroomController@index');
        $router->get('getPaginate', 'ClassroomController@getPaginate');
        $router->post('create', 'ClassroomController@create');
        $router->get('show/{id}', 'ClassroomController@show');
        $router->post('update/{id}', 'ClassroomController@update');
        $router->delete('remove/{id}', 'ClassroomController@remove');
        $router->post('searchAll', 'ClassroomController@searchAll');
    });
});

// Department
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'department',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('getPaginate', 'DepartmentController@getPaginate');
        $router->post('create', 'DepartmentController@create');
        $router->get('show/{id}', 'DepartmentController@show');
        $router->post('update/{id}', 'DepartmentController@update');
        $router->delete('remove/{id}', 'DepartmentController@remove');
        $router->post('searchAll', 'DepartmentController@searchAll');
    });
});

// Subject
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'subject',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'StudentController@index');
    $router->get('getPaginate', 'SubjectController@getPaginate');
    $router->post('create', 'SubjectController@create');
    $router->get('show/{id}', 'SubjectController@show');
    $router->post('update/{id}', 'SubjectController@update');
    $router->delete('remove/{id}', 'SubjectController@remove');
    $router->post('searchAll', 'SubjectController@searchAll');
    });
});

// OPTIONS

Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'department',
], function (Registrar $router) {
// Get options
    $router->get('list', 'DepartmentController@list');
    $router->get('getClassrooms/{id}', 'ClassroomController@getClassrooms');
    $router->get('getSubjects/{id}', 'SubjectController@getSubjects');
    $router->get('getSubjects/bundle/{departmentId}/{schoolYearId}', 'SubjectController@bundle');
//    $router->get('bundle/{id}', 'ClassroomController@bundle');
    $router->get('getTeacher/{id}', 'SubjectController@getTeacher');
});


// SchoolYear
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'schoolYear',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
//        $router->get('list', 'SchoolYearController@index');
        $router->get('getPaginate', 'SchoolYearController@getPaginate');
        $router->post('create', 'SchoolYearController@create');
        $router->get('show/{id}', 'SchoolYearController@show');
        $router->post('update/{id}', 'SchoolYearController@update');
        $router->delete('remove/{id}', 'SchoolYearController@remove');
        $router->post('searchAll', 'SchoolYearController@searchAll');
    });
});

// SchoolYear
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'schoolYear',
], function (Registrar $router) {
    $router->get('list', 'SchoolYearController@index');
});

// Student
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'student',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'StudentController@index');
        $router->get('getPaginate', 'StudentController@getPaginate');
        $router->post('create', 'StudentController@create');
        $router->get('show/{id}', 'StudentController@show');
        $router->post('update/{id}', 'StudentController@update');
        $router->delete('remove/{id}', 'StudentController@remove');
        $router->post('searchAll', 'StudentController@searchAll');
        $router->post('upload', 'StudentController@upload');
    });
});

// Teacher
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'teacher',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'TeacherController@index');
        $router->get('getPaginate', 'TeacherController@getPaginate');
        $router->post('create', 'TeacherController@create');
        $router->get('show/{id}', 'TeacherController@show');
        $router->post('update/{id}', 'TeacherController@update');
        $router->delete('remove/{id}', 'TeacherController@remove');
        $router->post('searchAll', 'TeacherController@searchAll');
        $router->post('upload', 'TeacherController@upload');
    });
});

// CLASSROOM STUDENT
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'classroomStudent',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('getPaginate', 'ClassroomStudentController@getPaginate');
    $router->post('create', 'ClassroomStudentController@create');
    $router->get('show/{id}', 'ClassroomStudentController@show');
    $router->post('update/{id}', 'ClassroomStudentController@update');
    $router->delete('remove/{id}', 'ClassroomStudentController@remove');
    $router->post('searchAll', 'ClassroomStudentController@searchAll');
    });
});

// SCORECARD
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'scorecard',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list', 'ScorecardController@index');
    $router->post('create', 'ScorecardController@create');
    $router->get('show/{id}', 'ScorecardController@show');
    $router->post('update/{id}', 'ScorecardController@update');
    $router->delete('remove/{id}', 'ScorecardController@remove');
    $router->post('searchAll', 'ScorecardController@searchAll');

    // Test
    $router->post('getListSearch', 'ScorecardController@getListSearch');
//    });
});

//Route::group([
//    'namespace' => 'Api\\AdminApi',
//    'prefix'    => 'scorecard',
//], function (Registrar $router) {
//        // Test
//        $router->post('getListSearch', 'ScorecardController@getListSearch');
//});

// TRAINING POINT
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'trainingPoint',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list', 'TrainingPointController@index');
    $router->post('create', 'TrainingPointController@create');
    $router->get('show/{id}', 'TrainingPointController@show');
    $router->post('update/{id}', 'TrainingPointController@update');
    $router->delete('remove/{id}', 'TrainingPointController@remove');
    $router->post('searchAll', 'TrainingPointController@searchAll');

    // Test
    $router->post('getListSearch', 'TrainingPointController@getListSearch');
    });
});

// CLASSIFICATION
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'classification',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list', 'ClassificationController@index');
    $router->post('create', 'ClassificationController@create');
    $router->get('show/{id}', 'ClassificationController@show');
    $router->post('update/{id}', 'ClassificationController@update');
    $router->delete('remove/{id}', 'ClassificationController@remove');
    $router->post('searchAll', 'ClassificationController@searchAll');

    // Test
    $router->post('getListSearch', 'ClassificationController@getListSearch');
    });
});

// Attendance
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'attendance',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list', 'AttendanceController@index');
    $router->post('create', 'AttendanceController@create');
    $router->get('show/{id}', 'AttendanceController@show');
    $router->post('update/{id}', 'AttendanceController@update');
    $router->delete('remove/{id}', 'AttendanceController@remove');
    $router->post('searchAll', 'AttendanceController@searchAll');

    // Test
    $router->post('getListSearch', 'AttendanceController@getListSearch');
//    });
});

//Route::group([
//    'namespace' => 'Api\\AdminApi',
//    'prefix'    => 'attendance',
//], function (Registrar $router) {
//    // Test
//    $router->post('getListSearch', 'AttendanceController@getListSearch');
//
//});

// SUBJECT TEACHER
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'subjectTeacher',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('getPaginate', 'SubjectTeacherController@getPaginate');
    $router->post('create', 'SubjectTeacherController@create');
    $router->get('show/{id}', 'SubjectTeacherController@show');
    $router->post('update/{id}', 'SubjectTeacherController@update');
    $router->delete('remove/{id}', 'SubjectTeacherController@remove');
    $router->post('searchAll', 'SubjectTeacherController@searchAll');
    });
});

// NEWS
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'news',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'NewsController@index');
        $router->post('create', 'NewsController@create');
        $router->get('show/{id}', 'NewsController@show');
        $router->post('update/{id}', 'NewsController@update');
        $router->delete('remove/{id}', 'NewsController@remove');
        $router->post('searchAll', 'NewsController@searchAll');
        $router->post('upload', 'NewsController@upload');
    });
});

// CONTACT
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'contact',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
    $router->get('list', 'ContactController@index');
    $router->delete('remove/{id}', 'ContactController@remove');
    $router->post('searchAll', 'ContactController@searchAll');
    });
});

// Thống kê số liệu
Route::group([
    'namespace' => 'Api\\AdminApi',
    'prefix'    => 'statistical',
], function (Registrar $router) {
    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {
        $router->get('list', 'StatisticalController@index');
        $router->post('searchOptions', 'StatisticalController@searchOptions');
        $router->post('searchhk3', 'StatisticalController@searchhk3');
    });
});


//=====================================================================================
// SCORECARD OF CUSTOMERAPI
Route::group([
    'namespace' => 'Api\\CustomerApi',
    'prefix'    => 'scorecardOfCustomer',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list/{code_number_teacher}', 'ScorecardController@index');
    $router->post('create', 'ScorecardController@create');
    $router->get('show/{id}', 'ScorecardController@show');
    $router->post('update/{id}', 'ScorecardController@update');
    $router->delete('remove/{id}', 'ScorecardController@remove');
    $router->post('searchAll', 'ScorecardController@searchAll');

    // Test
    $router->post('getListSearch', 'ScorecardController@getListSearch');
//    });
});

// Attendance
Route::group([
    'namespace' => 'Api\\CustomerApi',
    'prefix'    => 'attendanceOfCustomer',
], function (Registrar $router) {
//    $router->group(['middleware' => ['auth:sanctum']], function (Registrar $router) {

    $router->get('list/{code_number_teacher}', 'AttendanceController@index');
    $router->post('create', 'AttendanceController@create');
    $router->get('show/{id}', 'AttendanceController@show');
    $router->post('update/{id}', 'AttendanceController@update');
    $router->delete('remove/{id}', 'AttendanceController@remove');
    $router->post('searchAll', 'AttendanceController@searchAll');

    // Test
    $router->post('getListSearch', 'AttendanceController@getListSearch');
//    });
});