<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Info extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_department',
        'code_number_class',
        'code_number_school_year',
        'code_number_subject',
        'code_number_student',
        'code_number_teacher',
    ];

    protected $filter = [
        'id',
        'code_number_department',
        'code_number_class',
        'code_number_school_year',
        'code_number_subject',
        'code_number_student',
        'code_number_teacher',
    ];

    public function departments()
    {
        return $this->belongsTo(Department::class, 'code_number_department', 'code_number_department');
    }

    public function schoolYears()
    {
        return $this->belongsTo(SchoolYear::class, 'code_number_school_year', 'id');
    }

    public function students()
    {
        return $this->belongsTo(Student::class, 'code_number_student', 'code_number_student');
    }

    public function subjects()
    {
        return $this->belongsTo(Student::class, 'code_number_subject', 'code_number_subject');
    }
}
