<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Classroom extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_department',
        'code_number_class',
        'name',
    ];

    protected $filter = [
        'id',
        'code_number_department',
        'code_number_class',
        'name',
    ];

//    public function students()
//    {
//        return $this->hasMany(Student::class, 'code_number_class', 'code_number_class');
//    }
}
