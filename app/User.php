<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, DataTablePaginate, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'password',
//        'email',
        'code_number_teacher',
        'code_number_student',
        'role',
        'status'
    ];

    protected $filter = [
        'id',
        'username',
        'password',
//        'email',
        'code_number_teacher',
        'code_number_student',
        'role',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function students()
    {
        return $this->belongsTo(Student::class, 'code_number_student', 'code_number_student');
    }

    public function teachers()
    {
        return $this->belongsTo(Teacher::class, 'code_number_teacher', 'code_number_teacher');
    }
}
