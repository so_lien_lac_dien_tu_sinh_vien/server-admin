<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'full_name',
        'email',
        'sex',
        'mobile',
        'description'
    ];

    protected $filter = [
        'id',
        'full_name',
        'email',
        'sex',
        'mobile',
        'description'
    ];
}
