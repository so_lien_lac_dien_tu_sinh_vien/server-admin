<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class SubjectTeacher extends Model
{
    use DataTablePaginate;

    protected $table = "subject_teachers";

    protected $fillable = [
        'code_number_subject',
        'code_number_teacher',
    ];

    protected $filter = [
        'id',
        'code_number_subject',
        'code_number_teacher',
    ];
}
