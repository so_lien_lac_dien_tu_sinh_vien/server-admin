<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_student',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',
        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',
        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',
        'full_name_father',
        'birthday_father',
        'mobile_father',
        'work_unit_father',
        'full_name_mother',
        'birthday_mother',
        'mobile_mother',
        'work_unit_mother',
    ];

    protected $filter = [
        'id',
        'code_number_student',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',
        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',
        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',
        'full_name_father',
        'birthday_father',
        'mobile_father',
        'work_unit_father',
        'full_name_mother',
        'birthday_mother',
        'mobile_mother',
        'work_unit_mother',
    ];
}
