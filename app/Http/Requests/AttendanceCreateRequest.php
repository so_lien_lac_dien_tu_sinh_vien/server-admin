<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttendanceCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_class'                                     => 'required',
            'code_number_school_year'                               => 'required',
            'code_number_subject'                                   => 'required',
            'code_number_student'                                   => 'required',
            'code_number_teacher'                                   => 'required',
            'date_absent'                                           => 'required',
            'absent'                                                => 'required',
//            'reason_for_absence'                                    => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'code_number_class.required'                            => 'Bạn chưa nhập mã lớp học',
            'code_number_school_year.required'                      => 'Bạn chưa nhập học kì năm học',
            'code_number_student.required'                          => 'Bạn chưa nhập mã sinh viên',
            'code_number_subject.required'                          => 'Bạn chưa nhập mã môn học',
            'code_number_teacher.required'                          => 'Bạn chưa nhập mã giảng viên',
            'date_absent.required'                                  => 'Bạn chưa nhập ngày vắng mặt',
            'absent.required'                                       => 'Bạn chưa nhập có/không phép vắng mặt',
        ];
    }
}
