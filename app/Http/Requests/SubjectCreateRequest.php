<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubjectCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_department'                                    => 'required',
            'code_number_class'                                         => 'required',
            'code_number_school_year'                                   => 'required',
            'code_number_subject'                                       => 'required',
            'code_number_teacher'                                       => 'required',
            'name'                                                      => 'required',
            'count_credit'                                              => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_department.required'                           => 'Bạn chưa nhập mã phòng ban',
            'code_number_class.required'                                => 'Bạn chưa nhập mã phòng học',
            'code_number_school_year.required'                          => 'Bạn chưa nhập mã năm học',
            'code_number_subject.required'                              => 'Bạn chưa nhập mã môn học',
            'code_number_teacher.required'                              => 'Bạn chưa nhập mã giảng viên',
            'name.required'                                             => 'Bạn chưa nhập tên môn học',
            'count_credit.required'                                     => 'Bạn chưa nhập số tín chỉ',
        ];
    }
}
