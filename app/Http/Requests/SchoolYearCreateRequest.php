<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SchoolYearCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'name_year'                                             => 'required',
            'name_semester'                                         => 'required',
        ];
    }

    public function messages()
    {
        return [
            'name_year.required'                                    => 'Bạn chưa nhập năm học',
            'name_semester.required'                                => 'Bạn chưa nhập học kì',
        ];
    }
}
