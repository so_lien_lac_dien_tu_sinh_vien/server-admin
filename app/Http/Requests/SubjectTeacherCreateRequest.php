<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubjectTeacherCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_subject'                                       => 'required',
            'code_number_teacher'                                       => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_subject.required'                              => 'Bạn chưa nhập mã môn học',
            'code_number_teacher.required'                              => 'Bạn chưa nhập mã giảng viên',
        ];
    }
}
