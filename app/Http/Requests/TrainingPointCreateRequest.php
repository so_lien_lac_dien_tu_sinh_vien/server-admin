<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TrainingPointCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_class'                                     => 'required',
            'code_number_school_year'                               => 'required',
            'code_number_student'                                   => 'required',
//            'code_number_teacher'                                   => 'required',
            'training_point'                                        => 'required|min:0|max:100'
        ];
    }

    public function messages()
    {
        return [
            'code_number_class.required'                            => 'Bạn chưa nhập mã lớp học',
            'code_number_school_year.required'                      => 'Bạn chưa nhập học kì năm học',
            'code_number_student.required'                          => 'Bạn chưa nhập mã sinh viên',
//            'code_number_teacher.required'                          => 'Bạn chưa nhập mã giảng viên',
            'training_point.required'                               => 'Bạn chưa nhập điểm rèn luyện',
            // 'training_point.numeric'                               => 'Bạn chưa nhập điểm rèn luyện',
            'training_point.min'                                    => 'Điểm rèn luyện ít nhất là 0',
            'training_point.max'                                    => 'Điểm rèn luyện tối đa là 100',
        ];
    }
}
