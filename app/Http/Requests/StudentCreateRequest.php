<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StudentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_student'                               => 'required',
            'slug'                                              => 'nullable',
            'last_name'                                         => 'required',
            'first_name'                                        => 'required',
            'sex'                                               => 'required',
            'birthday'                                          => 'required',
            'thumbnails'                                        => 'required',
            'email'                                             => 'required',
            'email_verified_at'                                 => 'nullable',
            'identity_number'                                   => 'required',
            'phone'                                             => 'required',
            'mobile'                                            => 'required',
            'mobile_verified_at'                                => 'nullable',
            'place_of_birth'                                    => 'required',
            'home_town'                                         => 'required',
            'permanent_residence'                               => 'required',
            'current_home'                                      => 'required',
            'nation'                                            => 'required',
            'religion'                                          => 'required',
            'nationality'                                       => 'required',
            'full_name_father'                                  => 'required',
            'birthday_father'                                   => 'required',
            'mobile_father'                                     => 'required',
            'work_unit_father'                                  => 'required',
            'full_name_mother'                                  => 'required',
            'birthday_mother'                                   => 'required',
            'mobile_mother'                                     => 'required',
            'work_unit_mother'                                  => 'required',


        ];
    }

    public function messages()
    {
        return [
            'code_number_student.required'                           => 'Bạn chưa nhập mã sinh viên',
//            'slug.required'                                          => 'Bạn chưa nhập tài khoản',
            'last_name.required'                                     => 'Bạn chưa nhập họ sinh viên',
            'first_name.required'                                    => 'Bạn chưa nhập tên sinh viên',
            'sex.required'                                           => 'Bạn chưa nhập giới tính',
            'birthday.required'                                      => 'Bạn chưa nhập ngày sinh',
            'thumbnails.required'                                    => 'Bạn chưa nhập ảnh đại diện',
            'email.required'                                         => 'Bạn chưa nhập địa chỉ email',
//            'email_verified_at.required'                             => 'Bạn chưa nhập tài khoản',
            'identity_number.required'                               => 'Bạn chưa nhập CMND',
            'phone.required'                                         => 'Bạn chưa nhập điện thoại bàn',
            'mobile.required'                                        => 'Bạn chưa nhập điện thoại di động',
//            'mobile_verified_at.required'                            => 'Bạn chưa nhập tài khoản',
            'place_of_birth.required'                                => 'Bạn chưa nhập nơi sinh',
            'home_town.required'                                     => 'Bạn chưa nhập quê quán',
            'permanent_residence.required'                           => 'Bạn chưa nhập hộ khẩu thường trú',
            'current_home.required'                                  => 'Bạn chưa nhập nơi ở hiện tại',
            'nation.required'                                        => 'Bạn chưa nhập dân tộc',
            'religion.required'                                      => 'Bạn chưa nhập tôn giáo',
            'nationality.required'                                   => 'Bạn chưa nhập quốc tịch',
            'full_name_father.required'                              => 'Bạn chưa nhập họ tên cha',
            'birthday_father.required'                               => 'Bạn chưa nhập ngày sinh của cha',
            'mobile_father.required'                                 => 'Bạn chưa nhập số điện thoại của cha',
            'work_unit_father.required'                              => 'Bạn chưa nhập đơn vị công tác của cha',
            'full_name_mother.required'                              => 'Bạn chưa nhập họ tên mẹ',
            'birthday_mother.required'                               => 'Bạn chưa nhập ngày sinh của mẹ',
            'mobile_mother.required'                                 => 'Bạn chưa nhập số điện thoại của mẹ',
            'work_unit_mother.required'                              => 'Bạn chưa nhập đơn vị công tác của mẹ',
        ];
    }
}
