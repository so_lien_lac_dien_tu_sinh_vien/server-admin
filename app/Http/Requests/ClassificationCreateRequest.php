<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassificationCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_class'                                     => 'required',
            'code_number_school_year'                               => 'required',
            'code_number_student'                                   => 'required',
//            'code_number_teacher'                                   => 'required',
            'classification'                                        => 'required'
        ];
    }

    public function messages()
    {
        return [
            'code_number_class.required'                            => 'Bạn chưa nhập mã lớp học',
            'code_number_school_year.required'                      => 'Bạn chưa nhập học kì năm học',
            'code_number_student.required'                          => 'Bạn chưa nhập mã sinh viên',
//            'code_number_teacher.required'                          => 'Bạn chưa nhập mã giảng viên',
            'classification.required'                               => 'Bạn chưa nhập xếp loại'
        ];
    }
}
