<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScorecardCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_class'                                     => 'required',
            'code_number_school_year'                               => 'required',
            'code_number_subject'                                   => 'required',
            'code_number_student'                                   => 'required',
            'code_number_teacher'                                   => 'required',
            'mid_score'                                             => 'required|min:0|max:10',
            'last_score'                                            => 'required|min:0|max:10',

        ];
    }

    public function messages()
    {
        return [
            'code_number_class.required'                            => 'Bạn chưa nhập mã lớp học',
            'code_number_school_year.required'                      => 'Bạn chưa nhập học kì năm học',
            'code_number_student.required'                          => 'Bạn chưa nhập mã sinh viên',
            'code_number_subject.required'                          => 'Bạn chưa nhập mã môn học',
            'code_number_teacher.required'                          => 'Bạn chưa nhập mã giảng viên',
            'mid_score.required'                                    => 'Bạn chưa nhập điểm giữa kì',
            'mid_score.min'                                         => 'Điểm giữa kì ít nhất là 0',
            'mid_score.max'                                         => 'Điểm giữa kì tối đa là 10',
            'last_score.required'                                   => 'Bạn chưa nhập điểm cuối kì',
            'last_score.min'                                        => 'Điểm cuối kì ít nhất là 0',
            'last_score.max'                                        => 'Điểm cuối kì tối đa là 10',
        ];
    }
}
