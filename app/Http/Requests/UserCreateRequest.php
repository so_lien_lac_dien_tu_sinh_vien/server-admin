<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'username'                                      => 'required',
            'password'                                      => 'required',
//            'email'                                         => 'required',
            'code_number_teacher'                           => 'nullable',
            'code_number_student'                           => 'nullable',
            'role'                                          => 'nullable',
            'status'                                        => 'nullable'
        ];
    }

    public function messages()
    {
        return [
            'username.required'                             => 'Bạn chưa nhập tài khoản',
            'password.required'                             => 'Bạn chưa nhập mật khẩu',
//            'email.required'                                => 'Bạn chưa nhập email'
        ];
    }
}
