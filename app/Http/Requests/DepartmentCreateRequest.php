<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepartmentCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_department'                                => 'required',
            'name'                                                  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_department.required'                       => 'Bạn chưa nhập mã phòng ban',
            'name.required'                                         => 'Bạn chưa nhập tên phòng ban',
        ];
    }
}
