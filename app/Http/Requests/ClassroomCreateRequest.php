<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClassroomCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Thông tin cơ bản
            'code_number_department'                                => 'required',
            'code_number_class'                                     => 'required',
            'name'                                                  => 'required',
        ];
    }

    public function messages()
    {
        return [
            'code_number_department.required'                       => 'Bạn chưa nhập mã phòng ban',
            'code_number_class.required'                            => 'Bạn chưa nhập mã lớp học',
            'name.required'                                         => 'Bạn chưa nhập tên lớp học',
        ];
    }
}
