<?php

namespace App\Http\Controllers\Api\CustomerApi;
use App\Http\Controllers\AbstractApiController;

use App\ClassroomStudent;
use App\Http\Requests\ScorecardCreateRequest;
use App\Scorecard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScorecardController extends AbstractApiController
{
    public function getListSearch(Request $request)
    {
        $searchOptionClass = $request->keyOptionClass;
        if($searchOptionClass) {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->where('code_number_class', '=', $searchOptionClass)
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
        else {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
    }

    public function index(Request $request, $code_number_teacher)
    {
        $scorecard = Scorecard::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_subject',
                'code_number_student',
                'code_number_teacher',
                'mid_score',
                'last_score',
                'medium_score'
            ])
            ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
            ->where('code_number_teacher', '=', $code_number_teacher)
            ->DataTablePaginate($request);

        return $this->item($scorecard);
    }

    public function create(ScorecardCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['code_number_student'] as $item) {
            $payload['code_number_class']                               = $validatedData['code_number_class'];
            $payload['code_number_school_year']                         = $validatedData['code_number_school_year'];
            $payload['code_number_subject']                             = $validatedData['code_number_subject'];
            $payload['code_number_student']                             = $item['code_number_student'];
            $payload['code_number_teacher']                             = $validatedData['code_number_teacher'];
            $payload['mid_score']                                       = $item['mid_score'];
            $payload['last_score']                                      = $item['last_score'];
            $payload['medium_score']                                    = ((float)$item['mid_score'] + (float)$item['last_score'])/2;

            // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['code_number_student'])) {
//            $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

            $check_scorecard = scorecard::query()
                ->where('code_number_student', '=' ,$payload['code_number_student'])
                ->where('code_number_subject', '=' ,$payload['code_number_subject'])
                ->where('code_number_school_year', '=' ,$payload['code_number_school_year'])
                ->first();

            if($check_scorecard) {
                $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
                $this->setStatusCode(400);
                return $this->respond();
            }

            // Tạo và lưu tài khoản
            $scorecard = Scorecard::create($payload);
            DB::beginTransaction();

            try {
                $scorecard->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm điểm thành công!');
                $this->setStatusCode(200);
                $this->setData($scorecard);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Scorecard::findOrFail($id);
    }

    public function update(ScorecardCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $scorecard = Scorecard::query()->findOrFail($id);
        if (! $scorecard) {
            $this->setMessage('Không có lớp học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $scorecard->code_number_class                                   = $validatedData['code_number_class'];
                $scorecard->code_number_school_year                             = $validatedData['code_number_school_year'];
                $scorecard->code_number_subject                                 = $validatedData['code_number_subject'];
                $scorecard->code_number_student                                 = $validatedData['code_number_student'];
                $scorecard->code_number_teacher                                 = $validatedData['code_number_teacher'];
                $scorecard->mid_score                                           = $validatedData['mid_score'];
                $scorecard->last_score                                          = $validatedData['last_score'];
                $scorecard->medium_score                                        = ((float)$validatedData['mid_score'] + (float)$validatedData['last_score'])/2;

                $scorecard->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($scorecard);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Scorecard::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
//    private function checkDuplicateName($code_number_student)
//    {
//        $scorecard = Scorecard::query()->get();
//        foreach ($scorecard->pluck('code_number_student') as $item) {
//            if ($code_number_student == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $scorecard = Scorecard::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_subject',
                'code_number_student',
                'code_number_teacher',
                'mid_score',
                'last_score',
                'medium_score'
            ])
            ->with('students', 'subjects', 'classrooms', 'schoolYears', 'teachers')
            ->whereHas('schoolYears', function($school_year) use($search) {
                $school_year->where('name_year', 'LIKE', "%$search%");
                $school_year->orWhere('name_semester', 'LIKE', "%$search%");
            })
            ->orWhereHas('subjects', function($subject) use($search) {
                $subject->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('students', function($student) use($search) {
                $student->where('first_name', 'LIKE', "%$search%");
                $student->orWhere('last_name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_class', 'LIKE', "%$search%")
            ->orWhere('code_number_subject', 'LIKE', "%$search%")
            ->orWhere('code_number_teacher', 'LIKE', "%$search%")
            ->orWhere('code_number_school_year', 'LIKE', "%$search%")
            ->orWhere('mid_score', 'LIKE', "%$search%")
            ->orWhere('last_score', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($scorecard);
    }
}
