<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Classroom;
use App\Http\Requests\ClassroomCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassroomController extends AbstractApiController
{
    public function index()
    {
        $classrooms = Classroom::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'name',
            ])
            ->get();

        return $this->item($classrooms);
    }

    public function getClassrooms($id)
    {
        $classrooms = Classroom::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'name',
            ])
            ->where('code_number_department', '=', $id)
            ->get();

        return $this->item($classrooms);
    }

//    public function bundle($id)
//    {
//        $query = Classroom::query();
//        $query->select([
//            'id',
//            'code_number_department',
//            'code_number_class',
//            'name',
//        ]);
//        $query->where('code_number_class', '=', $id);
//        $query->with('students');
//
//        $classroom = $query->firstOrFail();
//
//        return $this->item($classroom);
//    }

    public function getPaginate(Request $request)
    {
        $classroom = Classroom::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'name',
            ])
            ->DataTablePaginate($request);

        return $this->item($classroom);
    }

    public function create(ClassroomCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_department']                      = $validatedData['code_number_department'];
        $payload['code_number_class']                           = $validatedData['code_number_class'];
        $payload['name']                                        = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên lớp học');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $classroom = Classroom::create($payload);
        DB::beginTransaction();

        try {
            $classroom->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm lớp học thành công!');
            $this->setStatusCode(200);
            $this->setData($classroom);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Classroom::findOrFail($id);
    }

    public function update(ClassroomCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $classroom = Classroom::query()->findOrFail($id);
        if (! $classroom) {
            $this->setMessage('Không có lớp học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $classroom->code_number_department                 = $validatedData['code_number_department'];
                $classroom->code_number_class                      = $validatedData['code_number_class'];
                $classroom->name                                   = $validatedData['name'];

                $classroom->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($classroom);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Classroom::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $classroom = Classroom::query()->get();
        foreach ($classroom->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $classroom = Classroom::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'name',
            ])
            ->where('code_number_class', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($classroom);
    }
}
