<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\SubjectCreateRequest;
use App\Subject;
use App\SubjectTeacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectController extends AbstractApiController
{
    public function getSubjects($id)
    {
        $subjects = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->where('code_number_department', '=', $id)
            ->get();

        return $this->item($subjects);
    }

    public function getTeacher($id)
    {
        $subjects = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->where('code_number_subject', '=', $id)
            ->get();

        return $this->item($subjects);
    }

    public function bundle($departmentId, $schoolYearId)
    {
        $subjects = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->where('code_number_department', '=', $departmentId)
            ->where('code_number_school_year', '=', $schoolYearId)
            ->get();

        return $this->item($subjects);
    }

    public function index()
    {
        $subject = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->get();

        return $this->item($subject);
    }
    public function getPaginate(Request $request)
    {
        $subject = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->DataTablePaginate($request);

        return $this->item($subject);
    }

    public function create(SubjectCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_department']                      = $validatedData['code_number_department'];
        $payload['code_number_class']                           = $validatedData['code_number_class'];
        $payload['code_number_subject']                         = $validatedData['code_number_subject'];
        $payload['code_number_school_year']                     = $validatedData['code_number_school_year'];
        $payload['code_number_teacher']                         = $validatedData['code_number_teacher'];
        $payload['name']                                        = $validatedData['name'];
        $payload['count_credit']                                = $validatedData['count_credit'];
        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên môn học');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $subject = Subject::create($payload);
        DB::beginTransaction();

        try {
            $subject->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm môn học thành công!');
            $this->setStatusCode(200);
            $this->setData($subject);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Subject::findOrFail($id);
    }

    public function update(SubjectCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $subject = Subject::query()->findOrFail($id);
        if (! $subject) {
            $this->setMessage('Không có môn học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $subject->code_number_department                      = $validatedData['code_number_department'];
                $subject->code_number_class                           = $validatedData['code_number_class'];
                $subject->code_number_subject                         = $validatedData['code_number_subject'];
                $subject->code_number_school_year                     = $validatedData['code_number_school_year'];
                $subject->code_number_teacher                         = $validatedData['code_number_teacher'];
                $subject->name                                        = $validatedData['name'];
                $subject->count_credit                                = $validatedData['count_credit'];

                $subject->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($subject);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Subject::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $subject = Subject::query()->get();
        foreach ($subject->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $subject = Subject::query()
            ->select([
                'id',
                'code_number_department',
                'code_number_class',
                'code_number_subject',
                'code_number_school_year',
                'code_number_teacher',
                'name',
                'count_credit'
            ])
            ->with('teachers','schoolYears')
            ->whereHas('schoolYears', function($school_year) use($search) {
                $school_year->where('name_year', 'LIKE', "%$search%");
                $school_year->orWhere('name_semester', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_subject', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->orWhere('code_number_class', 'LIKE', "%$search%")
            ->orWhere('code_number_department', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($subject);
    }
}
