<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\StudentCreateRequest;
use App\Student;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Cocur\Slugify\Slugify;

class StudentController extends AbstractApiController
{
    public function index()
    {
        $student = Student::query()
            ->select([
                'id',
                'code_number_student',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'full_name_father',
                'birthday_father',
                'mobile_father',
                'work_unit_father',
                'full_name_mother',
                'birthday_mother',
                'mobile_mother',
                'work_unit_mother',
            ])
            ->get();

        return $this->item($student);
    }

    public function getPaginate(Request $request)
    {
        $student = Student::query()
            ->select([
                'id',
                'code_number_student',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'full_name_father',
                'birthday_father',
                'mobile_father',
                'work_unit_father',
                'full_name_mother',
                'birthday_mother',
                'mobile_mother',
                'work_unit_mother',
            ])
            ->DataTablePaginate($request);

        return $this->item($student);
    }

    public function create(StudentCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $payload = [];

        $payload['code_number_student']                         = $validatedData['code_number_student'];
        $payload['slug']                                        = $slugify->slugify($validatedData['code_number_student']);
        $payload['last_name']                                   = $validatedData['last_name'];
        $payload['first_name']                                  = $validatedData['first_name'];
        $payload['sex']                                         = $validatedData['sex'];
        $payload['birthday']                                    = $validatedData['birthday'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['email']                                       = $validatedData['email'];
        $payload['email_verified_at']                           = Carbon::now();
        $payload['identity_number']                             = $validatedData['identity_number'];
        $payload['phone']                                       = $validatedData['phone'];
        $payload['mobile']                                      = $validatedData['mobile'];
        $payload['mobile_verified_at']                          = Carbon::now();
        $payload['place_of_birth']                              = $validatedData['place_of_birth'];
        $payload['home_town']                                   = $validatedData['home_town'];
        $payload['permanent_residence']                         = $validatedData['permanent_residence'];
        $payload['current_home']                                = $validatedData['current_home'];
        $payload['nation']                                      = $validatedData['nation'];
        $payload['religion']                                    = $validatedData['religion'];
        $payload['nationality']                                 = $validatedData['nationality'];
        $payload['full_name_father']                            = $validatedData['full_name_father'];
        $payload['birthday_father']                             = $validatedData['birthday_father'];
        $payload['mobile_father']                               = $validatedData['mobile_father'];
        $payload['work_unit_father']                            = $validatedData['work_unit_father'];
        $payload['full_name_mother']                            = $validatedData['full_name_mother'];
        $payload['birthday_mother']                             = $validatedData['birthday_mother'];
        $payload['mobile_mother']                               = $validatedData['mobile_mother'];
        $payload['work_unit_mother']                            = $validatedData['work_unit_mother'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['code_number_student'])) {
            $this->setMessage('Đã tồn tại mã sinh viên');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $student = Student::create($payload);
        DB::beginTransaction();

        try {
            $student->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm sinh viên thành công!');
            $this->setStatusCode(200);
            $this->setData($student);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Student::findOrFail($id);
    }

    public function update(StudentCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $student = Student::query()->findOrFail($id);
        if (! $student) {
            $this->setMessage('Không có sinh viên này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật sinh viên
                $student->code_number_student                         = $validatedData['code_number_student'];
                $student->slug                                        = $slugify->slugify($validatedData['code_number_student']);
                $student->last_name                                   = $validatedData['last_name'];
                $student->first_name                                  = $validatedData['first_name'];
                $student->sex                                         = $validatedData['sex'];
                $student->birthday                                    = $validatedData['birthday'];
                $student->thumbnails                                  = $validatedData['thumbnails'];
                $student->email                                       = $validatedData['email'];
                $student->email_verified_at                           = $validatedData['email_verified_at'];
                $student->identity_number                             = $validatedData['identity_number'];
                $student->phone                                       = $validatedData['phone'];
                $student->mobile                                      = $validatedData['mobile'];
                $student->mobile_verified_at                          = $validatedData['mobile_verified_at'];
                $student->place_of_birth                              = $validatedData['place_of_birth'];
                $student->home_town                                   = $validatedData['home_town'];
                $student->permanent_residence                         = $validatedData['permanent_residence'];
                $student->current_home                                = $validatedData['current_home'];
                $student->nation                                      = $validatedData['nation'];
                $student->religion                                    = $validatedData['religion'];
                $student->nationality                                 = $validatedData['nationality'];
                $student->full_name_father                            = $validatedData['full_name_father'];
                $student->birthday_father                             = $validatedData['birthday_father'];
                $student->mobile_father                               = $validatedData['mobile_father'];
                $student->work_unit_father                            = $validatedData['work_unit_father'];
                $student->full_name_mother                            = $validatedData['full_name_mother'];
                $student->birthday_mother                             = $validatedData['birthday_mother'];
                $student->mobile_mother                               = $validatedData['mobile_mother'];
                $student->work_unit_mother                            = $validatedData['work_unit_mother'];

                $student->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($student);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Student::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_student)
    {
        $student = Student::query()->get();
        foreach ($student->pluck('code_number_student') as $item) {
            if ($code_number_student == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $student = Student::query()
            ->select([
                'id',
                'code_number_student',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'full_name_father',
                'birthday_father',
                'mobile_father',
                'work_unit_father',
                'full_name_mother',
                'birthday_mother',
                'mobile_mother',
                'work_unit_mother',
            ])
            ->where('code_number_student', 'LIKE', "%$search%")
            ->orWhere('last_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($student);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/student'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
