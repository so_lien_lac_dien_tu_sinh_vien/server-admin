<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\SubjectTeacherCreateRequest;
use App\SubjectTeacher;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SubjectTeacherController extends AbstractApiController
{
    public function list()
    {
        $subjectTeacher = SubjectTeacher::query()
            ->select([
                'id',
                'code_number_subject',
                'code_number_teacher'
            ])
            ->get();

        return $this->item($subjectTeacher);
    }

    public function bundle($id)
    {
        $query = SubjectTeacher::query();
        $query->select([
            'id',
            'code_number_subject',
            'code_number_teacher'
        ]);
        $query->where('code_number_class', '=', $id);
        $query->with('classroom');

        $subjectTeacher = $query->firstOrFail();

        return $this->item($subjectTeacher);
    }

    public function getPaginate(Request $request)
    {
        $subjectTeacher = SubjectTeacher::query()
            ->select([
                'id',
                'code_number_subject',
                'code_number_teacher'
            ])
            ->DataTablePaginate($request);

        return $this->item($subjectTeacher);
    }

    public function create(SubjectTeacherCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];



        foreach ($request['code_number_teacher'] as $item) {
            $payload['code_number_subject']                                     = $validatedData['code_number_subject'];
            $payload['code_number_teacher']                                     = $item['value'];


            // Tạo và lưu thành tích cá nhân
            $subjectTeacher = SubjectTeacher::create($payload);
            DB::beginTransaction();

            try {
                $subjectTeacher->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm môn học cho giảng viên thành công!');
                $this->setStatusCode(200);
                $this->setData($subjectTeacher);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }











//        $payload['code_number_class']                                       = $validatedData['code_number_class'];
//        $payload['code_number_student']                                     = $validatedData['code_number_student'];

        // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['name'])) {
//            $this->setMessage('Đã tồn tại tên phòng ban');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

        //Tạo và lưu tài khoản

        return $this->respond();
    }

    public function show($id)
    {
        return SubjectTeacher::findOrFail($id);
    }

    public function update(SubjectTeacherCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $subjectTeacher = SubjectTeacher::query()->findOrFail($id);
        if (! $subjectTeacher) {
            $this->setMessage('Không có môn học cho giảng viên này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $subjectTeacher->code_number_subject                          = $validatedData['code_number_subject'];
                $subjectTeacher->code_number_teacher                          = $validatedData['code_number_teacher'];

                $subjectTeacher->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($subjectTeacher);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        SubjectTeacher::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

//    private function checkDuplicateName($name)
//    {
//        $classroomStudent = ClassroomStudent::query()->get();
//        foreach ($classroomStudent->pluck('name') as $item) {
//            if ($name == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $subjectTeacher = SubjectTeacher::query()
            ->select([
                'id',
                'code_number_subject',
                'code_number_teacher'
            ])
            ->where('code_number_subject', 'LIKE', "%$search%")
            ->orWhere('code_number_teacher', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($subjectTeacher);
    }
}
