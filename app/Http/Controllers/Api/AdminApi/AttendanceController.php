<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Http\Controllers\AbstractApiController;

use App\Attendance;
use App\ClassroomStudent;
use App\Http\Requests\AttendanceCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AttendanceController extends AbstractApiController
{
    public function getListSearch(Request $request)
    {
        $searchOptionClass = $request->keyOptionClass;
        if($searchOptionClass) {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->where('code_number_class', '=', $searchOptionClass)
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
        else {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
    }

    public function index(Request $request)
    {
        $attendance = Attendance::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_subject',
                'code_number_student',
                'code_number_teacher',
                'date_absent',
                'absent',
                'reason_for_absence'
            ])
            ->with('students', 'subjects', 'classrooms', 'schoolYears')
            ->DataTablePaginate($request);

        return $this->item($attendance);
    }

    public function create(AttendanceCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['code_number_student'] as $item) {
            $payload['code_number_class']                               = $validatedData['code_number_class'];
            $payload['code_number_school_year']                         = $validatedData['code_number_school_year'];
            $payload['code_number_subject']                             = $validatedData['code_number_subject'];
            $payload['code_number_student']                             = $item['code_number_student'];
            $payload['code_number_teacher']                             = $validatedData['code_number_teacher'];
            $payload['date_absent']                                     = $validatedData['date_absent'];
//            $payload['absent']                                          = $item['absent'];
            $payload['absent']                                          = ! empty($item['absent']) ? $item['absent'] : 0;
            $payload['reason_for_absence']                              = ! empty($item['reason_for_absence']) ? $item['reason_for_absence'] : "";

            // Kiểm tra trùng tên danh mục
//            if (! $this->checkDuplicateName($payload['code_number_student'])) {
//                $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
//                $this->setStatusCode(400);
//                return $this->respond();
//            }

            $check_attendance = Attendance::query()
                ->where('code_number_student', '=' ,$payload['code_number_student'])
                ->where('code_number_subject', '=' ,$payload['code_number_subject'])
                ->where('date_absent', '=' ,$payload['date_absent'])
                ->first();

            if($check_attendance) {
                $this->setMessage('Sinh viên đã tồn tại điểm danh trong lớp học');
                $this->setStatusCode(400);
                return $this->respond();
            }

            // Tạo và lưu tài khoản
            $attendance = Attendance::create($payload);
            DB::beginTransaction();

            try {
                $attendance->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm vắng mặt thành công!');
                $this->setStatusCode(200);
                $this->setData($attendance);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Attendance::findOrFail($id);
    }

    public function update(AttendanceCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $attendance = Attendance::query()->findOrFail($id);
        if (! $attendance) {
            $this->setMessage('Không có vắng mặt này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $attendance->code_number_class                                   = $validatedData['code_number_class'];
                $attendance->code_number_school_year                             = $validatedData['code_number_school_year'];
                $attendance->code_number_subject                                 = $validatedData['code_number_subject'];
                $attendance->code_number_student                                 = $validatedData['code_number_student'];
//                $attendance->code_number_teacher                                 = $validatedData['code_number_teacher'];
                $attendance->code_number_teacher                                 = $validatedData['code_number_teacher'];
                $attendance->date_absent                                         = $validatedData['date_absent'];
                $attendance->absent                                              = ! empty($validatedData['absent']) ? $validatedData['absent'] : 0;
                $attendance->reason_for_absence                                  = ! empty($request->reason_for_absence) ? $request->reason_for_absence : "";

                $attendance->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($attendance);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Attendance::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_student)
    {
        $attendance = Attendance::query()->get();
        foreach ($attendance->pluck('code_number_student') as $item) {
            if ($code_number_student == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $attendance = Attendance::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_subject',
                'code_number_student',
                'code_number_teacher',
                'date_absent',
                'absent',
                'reason_for_absence'
            ])
            ->with('students', 'subjects', 'classrooms', 'schoolYears')
            ->whereHas('schoolYears', function($school_year) use($search) {
                $school_year->where('name_year', 'LIKE', "%$search%");
                $school_year->orWhere('name_semester', 'LIKE', "%$search%");
            })
            ->orWhereHas('subjects', function($subject) use($search) {
                $subject->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('students', function($student) use($search) {
                $student->where('first_name', 'LIKE', "%$search%");
                $student->orWhere('last_name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_class', 'LIKE', "%$search%")
            ->orWhere('reason_for_absence', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($attendance);
    }
}
