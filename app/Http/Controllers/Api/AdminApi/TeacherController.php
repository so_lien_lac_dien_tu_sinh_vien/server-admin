<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Http\Requests\TeacherCreateRequest;
use App\Teacher;
use Carbon\Carbon;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeacherController extends AbstractApiController
{
    public function index()
    {
        $teacher = Teacher::query()
            ->select([
                'id',
                'code_number_teacher',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
            ])
            ->get();

        return $this->item($teacher);
    }

    public function getPaginate(Request $request)
    {
        $teacher = Teacher::query()
            ->select([
                'id',
                'code_number_teacher',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
            ])
            ->DataTablePaginate($request);

        return $this->item($teacher);
    }

    public function create(TeacherCreateRequest $request)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $payload = [];

        $payload['code_number_teacher']                         = $validatedData['code_number_teacher'];
        $payload['slug']                                        = $slugify->slugify($validatedData['code_number_teacher']);
        $payload['last_name']                                   = $validatedData['last_name'];
        $payload['first_name']                                  = $validatedData['first_name'];
        $payload['sex']                                         = $validatedData['sex'];
        $payload['birthday']                                    = $validatedData['birthday'];
        $payload['thumbnails']                                  = $validatedData['thumbnails'];
        $payload['email']                                       = $validatedData['email'];
        $payload['email_verified_at']                           = Carbon::now();
        $payload['identity_number']                             = $validatedData['identity_number'];
        $payload['phone']                                       = $validatedData['phone'];
        $payload['mobile']                                      = $validatedData['mobile'];
        $payload['mobile_verified_at']                          = Carbon::now();
        $payload['place_of_birth']                              = $validatedData['place_of_birth'];
        $payload['home_town']                                   = $validatedData['home_town'];
        $payload['permanent_residence']                         = $validatedData['permanent_residence'];
        $payload['current_home']                                = $validatedData['current_home'];
        $payload['nation']                                      = $validatedData['nation'];
        $payload['religion']                                    = $validatedData['religion'];
        $payload['nationality']                                 = $validatedData['nationality'];
        $payload['level']                                       = $validatedData['level'];
        $payload['specialized']                                 = $validatedData['specialized'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['code_number_teacher'])) {
            $this->setMessage('Đã tồn tại mã sinh viên');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $teacher = Teacher::create($payload);
        DB::beginTransaction();

        try {
            $teacher->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm giảng viên thành công!');
            $this->setStatusCode(200);
            $this->setData($teacher);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Teacher::findOrFail($id);
    }

    public function update(TeacherCreateRequest $request, $id)
    {
        $validatedData = $request->validated();
        $slugify = new Slugify();

        $teacher = Teacher::query()->findOrFail($id);
        if (! $teacher) {
            $this->setMessage('Không có giảng viên này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $teacher->code_number_teacher                         = $validatedData['code_number_teacher'];
                $teacher->slug                                        = $slugify->slugify($validatedData['code_number_teacher']);
                $teacher->last_name                                   = $validatedData['last_name'];
                $teacher->first_name                                  = $validatedData['first_name'];
                $teacher->sex                                         = $validatedData['sex'];
                $teacher->birthday                                    = $validatedData['birthday'];
                $teacher->thumbnails                                  = $validatedData['thumbnails'];
                $teacher->email                                       = $validatedData['email'];
                $teacher->email_verified_at                           = $validatedData['email_verified_at'];
                $teacher->identity_number                             = $validatedData['identity_number'];
                $teacher->phone                                       = $validatedData['phone'];
                $teacher->mobile                                      = $validatedData['mobile'];
                $teacher->mobile_verified_at                          = $validatedData['mobile_verified_at'];
                $teacher->place_of_birth                              = $validatedData['place_of_birth'];
                $teacher->home_town                                   = $validatedData['home_town'];
                $teacher->permanent_residence                         = $validatedData['permanent_residence'];
                $teacher->current_home                                = $validatedData['current_home'];
                $teacher->nation                                      = $validatedData['nation'];
                $teacher->religion                                    = $validatedData['religion'];
                $teacher->nationality                                 = $validatedData['nationality'];
                $teacher->level                                       = $validatedData['level'];
                $teacher->specialized                                 = $validatedData['specialized'];

                $teacher->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($teacher);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Teacher::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($code_number_teacher)
    {
        $teacher = Teacher::query()->get();
        foreach ($teacher->pluck('code_number_teacher') as $item) {
            if ($code_number_teacher == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $teacher = Teacher::query()
            ->select([
                'id',
                'code_number_teacher',
                'slug',
                'last_name',
                'first_name',
                'sex',
                'birthday',
                'thumbnails',
                'email',
                'email_verified_at',
                'identity_number',
                'phone',
                'mobile',
                'mobile_verified_at',
                'place_of_birth',
                'home_town',
                'permanent_residence',
                'current_home',
                'nation',
                'religion',
                'nationality',
                'level',
                'specialized',
            ])
            ->where('code_number_teacher', 'LIKE', "%$search%")
            ->orWhere('last_name', 'LIKE', "%$search%")
            ->orWhere('first_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('mobile', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($teacher);
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('image')) {
            try {
                $Carbon = new Carbon();
                $theTime = Carbon::now()->format('Y-m-d');
                $theImageName = $theTime . '-' . $request->image->getClientOriginalName();
                $request->image->move(public_path('images/teacher'), $theImageName);

                $this->setMessage('Thêm ảnh thành công!');
                $this->setStatusCode(200);
                $this->setData($theImageName);
                return $this->respond();
            }
            catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }
}
