<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\SchoolYear;
use App\Http\Requests\SchoolYearCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SchoolYearController extends AbstractApiController
{
    public function index()
    {
        $schoolYear = SchoolYear::query()
            ->select([
                'id',
                'name_year',
                'name_semester',
            ])
            ->get();

        return $this->item($schoolYear);
    }

    public function getPaginate(Request $request)
    {
        $schoolYear = SchoolYear::query()
            ->select([
                'id',
                'name_year',
                'name_semester',
            ])
            ->DataTablePaginate($request);

        return $this->item($schoolYear);
    }

    public function create(SchoolYearCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['name_year']                                           = $validatedData['name_year'];
        $payload['name_semester']                                       = $validatedData['name_semester'];

        // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['name'])) {
//            $this->setMessage('Đã tồn tại tên lớp học');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

        // Tạo và lưu tài khoản
        $schoolYear = SchoolYear::create($payload);
        DB::beginTransaction();

        try {
            $schoolYear->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm học kì năm học thành công!');
            $this->setStatusCode(200);
            $this->setData($schoolYear);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return SchoolYear::findOrFail($id);
    }

    public function update(SchoolYearCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $schoolYear = SchoolYear::query()->findOrFail($id);
        if (! $schoolYear) {
            $this->setMessage('Không có học kì này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $schoolYear->name_year                                       = $validatedData['name_year'];
                $schoolYear->name_semester                                   = $validatedData['name_semester'];

                $schoolYear->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($schoolYear);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        SchoolYear::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
//    private function checkDuplicateName($name)
//    {
//        $classroom = Classroom::query()->get();
//        foreach ($classroom->pluck('name') as $item) {
//            if ($name == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $schoolYear = SchoolYear::query()
            ->select([
                'id',
                'name_year',
                'name_semester',
            ])
            ->where('name_year', 'LIKE', "%$search%")
            ->orWhere('name_semester', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($schoolYear);
    }
}
