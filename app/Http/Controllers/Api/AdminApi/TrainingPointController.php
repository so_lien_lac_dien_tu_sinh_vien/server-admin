<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\ClassroomStudent;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\TrainingPointCreateRequest;
use App\Scorecard;
use App\TrainingPoint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TrainingPointController extends AbstractApiController
{
    public function getListSearch(Request $request)
    {
        $searchOptionClass = $request->keyOptionClass;
        if($searchOptionClass) {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->where('code_number_class', '=', $searchOptionClass)
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
        else {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
    }

    public function index(Request $request)
    {
        $trainingPoint = TrainingPoint::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_student',
                'code_number_teacher',
                'training_point'
            ])
            ->with('students', 'classrooms', 'schoolYears', 'teachers')
            ->DataTablePaginate($request);

        return $this->item($trainingPoint);
    }

    public function create(TrainingPointCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['code_number_student'] as $item) {
            $payload['code_number_class']                               = $validatedData['code_number_class'];
            $payload['code_number_school_year']                         = $validatedData['code_number_school_year'];
            $payload['code_number_student']                             = $item['code_number_student'];
//            $payload['code_number_teacher']                             = $validatedData['code_number_teacher'];
            $payload['code_number_teacher']                             = "GV001";
            $payload['training_point']                                  = $item['training_point'];

            // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['code_number_student'])) {
//            $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

            $trainingPoint = TrainingPoint::query()
                ->where('code_number_student', '=' ,$payload['code_number_student'])
                ->where('code_number_school_year', '=' ,$payload['code_number_school_year'])
                ->first();

            if($trainingPoint) {
                $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
                $this->setStatusCode(400);
                return $this->respond();
            }

            // Tạo và lưu tài khoản
            $trainingPoint = TrainingPoint::create($payload);
            DB::beginTransaction();

            try {
                $trainingPoint->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm điểm thành công!');
                $this->setStatusCode(200);
                $this->setData($trainingPoint);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return TrainingPoint::findOrFail($id);
    }

    public function update(TrainingPointCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $trainingPoint = TrainingPoint::query()->findOrFail($id);
        if (! $trainingPoint) {
            $this->setMessage('Không có lớp học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $trainingPoint->code_number_class                                   = $validatedData['code_number_class'];
                $trainingPoint->code_number_school_year                             = $validatedData['code_number_school_year'];
                $trainingPoint->code_number_student                                 = $validatedData['code_number_student'];
//                $trainingPoint->code_number_teacher                                 = $validatedData['code_number_teacher'];
                $trainingPoint->code_number_teacher                                 = "GV001";
                $trainingPoint->training_point                                      = $validatedData['training_point'];

                $trainingPoint->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($trainingPoint);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        TrainingPoint::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
//    private function checkDuplicateName($code_number_student)
//    {
//        $scorecard = Scorecard::query()->get();
//        foreach ($scorecard->pluck('code_number_student') as $item) {
//            if ($code_number_student == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $scorecard = Scorecard::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_student',
                'code_number_teacher',
                'training_point'
            ])
            ->with('students', 'classrooms', 'schoolYears', 'teachers')
            ->whereHas('schoolYears', function($school_year) use($search) {
                $school_year->where('name_year', 'LIKE', "%$search%");
                $school_year->orWhere('name_semester', 'LIKE', "%$search%");
            })
            ->orWhereHas('students', function($student) use($search) {
                $student->where('first_name', 'LIKE', "%$search%");
                $student->orWhere('last_name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_class', 'LIKE', "%$search%")
            ->orWhere('code_number_teacher', 'LIKE', "%$search%")
            ->orWhere('code_number_school_year', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($scorecard);
    }
}
