<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Http\Controllers\AbstractApiController;

use App\ClassroomStudent;
use App\Http\Requests\ClassroomStudentCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassroomStudentController extends AbstractApiController
{
    public function list()
    {
        $classroomStudent = ClassroomStudent::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_student'
            ])
            ->get();

        return $this->item($classroomStudent);
    }

    public function bundle($id)
    {
        $query = ClassroomStudent::query();
        $query->select([
            'id',
            'code_number_class',
            'code_number_student'
        ]);
        $query->where('code_number_class', '=', $id);
        $query->with('classroom');

        $classroomStudent = $query->firstOrFail();

        return $this->item($classroomStudent);
    }

    public function getPaginate(Request $request)
    {
        $classroomStudent = ClassroomStudent::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_student'
            ])
            ->DataTablePaginate($request);

        return $this->item($classroomStudent);
    }

    public function create(ClassroomStudentCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];



        foreach ($request['code_number_student'] as $item) {
            $payload['code_number_class']                                       = $validatedData['code_number_class'];
            $payload['code_number_student']                                     = $item['value'];


            // Tạo và lưu thành tích cá nhân
            $classroomStudent = ClassroomStudent::create($payload);
            DB::beginTransaction();

            try {
                $classroomStudent->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm sinh viên vào lớp học thành công!');
                $this->setStatusCode(200);
                $this->setData($classroomStudent);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }











//        $payload['code_number_class']                                       = $validatedData['code_number_class'];
//        $payload['code_number_student']                                     = $validatedData['code_number_student'];

        // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['name'])) {
//            $this->setMessage('Đã tồn tại tên phòng ban');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

        //Tạo và lưu tài khoản

        return $this->respond();
    }

    public function show($id)
    {
        return ClassroomStudent::findOrFail($id);
    }

    public function update(ClassroomStudentCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $classroomStudent = ClassroomStudent::query()->findOrFail($id);
        if (! $classroomStudent) {
            $this->setMessage('Không có sinh viên thuộc lớp học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $classroomStudent->code_number_class                            = $validatedData['code_number_class'];
                $classroomStudent->code_number_student                          = $validatedData['code_number_student'];

                $classroomStudent->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($classroomStudent);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        ClassroomStudent::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

//    private function checkDuplicateName($name)
//    {
//        $classroomStudent = ClassroomStudent::query()->get();
//        foreach ($classroomStudent->pluck('name') as $item) {
//            if ($name == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $classroomStudent = ClassroomStudent::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_student',
            ])
            ->where('code_number_class', 'LIKE', "%$search%")
            ->orWhere('code_number_student', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($classroomStudent);
    }
}
