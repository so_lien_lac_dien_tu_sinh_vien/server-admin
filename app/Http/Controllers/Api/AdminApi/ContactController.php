<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;

use App\Contact;
use Illuminate\Http\Request;

class ContactController extends AbstractApiController
{
    public function index(Request $request)
    {
        $contact = Contact::query()
            ->select([
                'id',
                'full_name',
                'email',
                'sex',
                'mobile',
                'description'
            ])
            ->DataTablePaginate($request);

        return $this->item($contact);
    }

    public function remove($id)
    {
        Contact::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $contact = Contact::query()
            ->select([
                'id',
                'full_name',
                'email',
                'sex',
                'mobile',
                'description'
            ])
            ->where('full_name', 'LIKE', "%$search%")
            ->orWhere('email', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($contact);
    }
}
