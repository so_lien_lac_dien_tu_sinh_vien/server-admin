<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Classification;
use App\ClassroomStudent;
use App\Http\Controllers\AbstractApiController;

use App\Http\Requests\ClassificationCreateRequest;
use App\Scorecard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassificationController extends AbstractApiController
{
    public function getListSearch(Request $request)
    {
        $searchOptionClass = $request->keyOptionClass;
        if($searchOptionClass) {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->where('code_number_class', '=', $searchOptionClass)
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
        else {
            $classroomStudent = ClassroomStudent::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_student',
                ])
                ->with('students', 'classrooms')
                ->DataTablePaginate($request);

            return $this->item($classroomStudent);
        }
    }

    public function index(Request $request)
    {
        $classification = Classification::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_student',
                'classification'
            ])
            ->with('students', 'classrooms', 'schoolYears')
            ->DataTablePaginate($request);

        return $this->item($classification);
    }

    public function create(ClassificationCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        foreach ($request['code_number_student'] as $item) {
            $payload['code_number_class']                               = $validatedData['code_number_class'];
            $payload['code_number_school_year']                         = $validatedData['code_number_school_year'];
            $payload['code_number_student']                             = $item['code_number_student'];
            $payload['classification']                                  = $item['classification'];

            // Kiểm tra trùng tên danh mục
//        if (! $this->checkDuplicateName($payload['code_number_student'])) {
//            $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
//            $this->setStatusCode(400);
//            return $this->respond();
//        }

            $classification = Classification::query()
                ->where('code_number_student', '=' ,$payload['code_number_student'])
                ->where('code_number_school_year', '=' ,$payload['code_number_school_year'])
                ->first();

            if($classification) {
                $this->setMessage('Sinh viên đã tồn tại điểm trong lớp học');
                $this->setStatusCode(400);
                return $this->respond();
            }

            // Tạo và lưu tài khoản
            $classification = Classification::create($payload);
            DB::beginTransaction();

            try {
                $classification->save();
                DB::commit();
                // Trả kết quả
                $this->setMessage('Thêm điểm thành công!');
                $this->setStatusCode(200);
                $this->setData($classification);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();
                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Classification::findOrFail($id);
    }

    public function update(ClassificationCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $classification = Classification::query()->findOrFail($id);
        if (! $classification) {
            $this->setMessage('Không có lớp học này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $classification->code_number_class                                   = $validatedData['code_number_class'];
                $classification->code_number_school_year                             = $validatedData['code_number_school_year'];
                $classification->code_number_student                                 = $validatedData['code_number_student'];
                $classification->classification                                      = $validatedData['classification'];

                $classification->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($classification);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Classification::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
//    private function checkDuplicateName($code_number_student)
//    {
//        $scorecard = Scorecard::query()->get();
//        foreach ($scorecard->pluck('code_number_student') as $item) {
//            if ($code_number_student == $item) {
//                return false;
//            }
//        }
//        return true;
//    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $classification = Classification::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_student',
                'classification'
            ])
            ->with('students', 'classrooms', 'schoolYears')
            ->whereHas('schoolYears', function($school_year) use($search) {
                $school_year->where('name_year', 'LIKE', "%$search%");
                $school_year->orWhere('name_semester', 'LIKE', "%$search%");
            })
            ->orWhereHas('classrooms', function($sclassroom) use($search) {
                $sclassroom->where('name', 'LIKE', "%$search%");
            })
            ->orWhereHas('students', function($student) use($search) {
                $student->where('first_name', 'LIKE', "%$search%");
                $student->orWhere('last_name', 'LIKE', "%$search%");
            })
            ->orWhere('code_number_class', 'LIKE', "%$search%")
            ->orWhere('code_number_school_year', 'LIKE', "%$search%")
            ->orWhere('classification', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($classification);
    }
}
