<?php

namespace App\Http\Controllers\Api\AdminApi;

use App\Http\Controllers\AbstractApiController;
use App\Department;
use App\Http\Requests\DepartmentCreateRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DepartmentController extends AbstractApiController
{
    public function list()
    {
        $department = Department::query()
            ->select([
                'id',
                'code_number_department',
                'name'
            ])
//            ->with('schools')
            ->get();

        return $this->item($department);
    }

    public function getPaginate(Request $request)
    {
        $department = Department::query()
            ->select([
                'id',
                'code_number_department',
                'name',
            ])
            ->DataTablePaginate($request);

        return $this->item($department);
    }

    public function create(DepartmentCreateRequest $request)
    {
        $validatedData = $request->validated();
        $payload = [];

        $payload['code_number_department']                      = $validatedData['code_number_department'];
        $payload['name']                                        = $validatedData['name'];

        // Kiểm tra trùng tên danh mục
        if (! $this->checkDuplicateName($payload['name'])) {
            $this->setMessage('Đã tồn tại tên phòng ban');
            $this->setStatusCode(400);
            return $this->respond();
        }

        // Tạo và lưu tài khoản
        $department = Department::create($payload);
        DB::beginTransaction();

        try {
            $department->save();
            DB::commit();
            // Trả kết quả
            $this->setMessage('Thêm phòng ban thành công!');
            $this->setStatusCode(200);
            $this->setData($department);
        } catch (Exception $e) {
            report($e);
            DB::rollBack();
            // Thông báo lỗi
            $this->setMessage($e->getMessage());
            $this->setStatusCode(500);
        }
        return $this->respond();
    }

    public function show($id)
    {
        return Department::findOrFail($id);
    }

    public function update(DepartmentCreateRequest $request, $id)
    {
        $validatedData = $request->validated();

        $department = Department::query()->findOrFail($id);
        if (! $department) {
            $this->setMessage('Không có phòng ban này');
            $this->setStatusCode(400);
        } else {
            DB::beginTransaction();

            try {
                // Cập nhật
                $department->code_number_department                      = $validatedData['code_number_department'];
                $department->name                                        = $validatedData['name'];

                $department->save();
                DB::commit();

                // Trả về kết quả
                $this->setMessage('Cập nhật thành công');
                $this->setStatusCode(200);
                $this->setData($department);
            } catch (Exception $e) {
                report($e);
                DB::rollBack();

                // Thông báo lỗi
                $this->setMessage($e->getMessage());
                $this->setStatusCode(500);
            }
        }
        return $this->respond();
    }

    public function remove($id)
    {
        Department::findOrFail($id)->delete();
        return response()
            ->json(['message' => 'Success: Bạn đã xóa thành công!']);
    }

    /**
     * Kiểm tra trùng tên. Nếu trùng trả về false
     *
     * @param mixed $name
     */
    private function checkDuplicateName($name)
    {
        $department = Department::query()->get();
        foreach ($department->pluck('name') as $item) {
            if ($name == $item) {
                return false;
            }
        }
        return true;
    }

    public function searchAll(Request $request)
    {
        $search = $request->keyText;

        $department = Department::query()
            ->select([
                'id',
                'code_number_department',
                'name',
            ])
            ->where('code_number_department', 'LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->DataTablePaginate($request);
        return $this->item($department);
    }
}
