<?php

namespace App\Http\Controllers\Api\AdminApi;
use App\Http\Controllers\AbstractApiController;
use App\Classification;
use Illuminate\Http\Request;
use App\Subject;
use App\ClassroomStudent;

class StatisticalController extends AbstractApiController
{
    public function searchOptions(Request $request)
    {
        $school_year= $request->code_number_school_year;

        if($school_year == "null")
        {
            $classification = Classification::query()
                ->select([
                    'id',
                    'code_number_class',
                    'code_number_school_year',
                    'code_number_student',
                    'classification'
                ])
                ->with('students', 'classrooms', 'schoolYears')
                ->get();

            return $this->item($classification);
        }

        $classification = Classification::query()
            ->select([
                'id',
                'code_number_class',
                'code_number_school_year',
                'code_number_student',
                'classification'
            ])
            ->with('students', 'classrooms', 'schoolYears')
            ->whereHas('schoolYears', function($decisions) use($school_year) {
                $decisions->where('code_number_school_year', $school_year);
            })
            ->get();

        return $this->item($classification);
    }

    public function searchhk3(Request $request)
    {
        $school_year= $request->code_number_school_year;

        $student = Subject::query()
        ->where('code_number_school_year', '=', $school_year)
            ->get();


            $grouped = $student->groupBy(function ($item, $key) {
            return substr($item['code_number_class'], 0);
        });


        $SchoolYear1 = $grouped->map(function ($item, $key) {
            return $key;
        });

        foreach($SchoolYear1 as $item) {

        $listSV = ClassroomStudent::with('students')->where('code_number_class', '=', $item)->get();

        //         array_push($SchoolYear, [
        //             'id'  => $listSV,
        //         ]);
        }


        $tong = count($listSV);

        return $this->item([$listSV, $tong]);
    }
}
