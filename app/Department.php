<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_department',
        'name',
    ];

    protected $filter = [
        'id',
        'code_number_department',
        'name',
    ];

    public function classroom()
    {
        return $this->hasMany(Classroom::class, 'code_number_department', 'code_number_department');
    }
}
