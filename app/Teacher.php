<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_teacher',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',

        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',

        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',

        'level',
        'specialized',
    ];

    protected $filter = [
        'id',
        'code_number_teacher',
        'slug',
        'last_name',
        'first_name',
        'sex',
        'birthday',
        'thumbnails',

        'email',
        'email_verified_at',
        'identity_number',
        'phone',
        'mobile',
        'mobile_verified_at',

        'place_of_birth',
        'home_town',
        'permanent_residence',
        'current_home',
        'nation',
        'religion',
        'nationality',

        'level',
        'specialized',
    ];
}
