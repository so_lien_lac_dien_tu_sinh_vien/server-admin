<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_department',
        'code_number_subject',
        'code_number_school_year',
        'code_number_teacher',
        'code_number_class',
        'name',
        'count_credit'
    ];

    protected $filter = [
        'id',
        'code_number_department',
        'code_number_subject',
        'code_number_school_year',
        'code_number_teacher',
        'code_number_class',
        'name',
        'count_credit'
    ];

    public function schoolYears()
    {
        return $this->belongsTo(SchoolYear::class, 'code_number_school_year', 'id');
    }
    public function teachers()
    {
        return $this->belongsTo(Teacher::class, 'code_number_teacher', 'code_number_teacher');
    }

}
