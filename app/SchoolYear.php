<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class SchoolYear extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'name_year',
        'name_semester',
    ];

    protected $filter = [
        'id',
        'name_year',
        'name_semester',
    ];
}
