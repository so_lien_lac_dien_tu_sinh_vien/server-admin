<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class Scorecard extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_class',
        'code_number_school_year',
        'code_number_subject',
        'code_number_student',
        'code_number_teacher',
        'mid_score',
        'last_score',
        'medium_score'
    ];

    protected $filter = [
        'id',
        'code_number_class',
        'code_number_school_year',
        'code_number_subject',
        'code_number_student',
        'code_number_teacher',
        'mid_score',
        'last_score',
        'medium_score'
    ];

    public function students()
    {
        return $this->belongsTo(Student::class, 'code_number_student', 'code_number_student');
    }

    public function subjects()
    {
        return $this->belongsTo(Subject::class, 'code_number_subject', 'code_number_subject');
    }

    public function classrooms()
    {
        return $this->belongsTo(Classroom::class, 'code_number_class', 'code_number_class');
    }

    public function schoolYears()
    {
        return $this->belongsTo(SchoolYear::class, 'code_number_school_year', 'id');
    }

    public function teachers()
    {
        return $this->belongsTo(Teacher::class, 'code_number_teacher', 'code_number_teacher');
    }
}
