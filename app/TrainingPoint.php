<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class TrainingPoint extends Model
{
    use DataTablePaginate;

    protected $fillable = [
        'code_number_class',
        'code_number_school_year',
        'code_number_student',
        'code_number_teacher',
        'training_point'
    ];

    protected $filter = [
        'id',
        'code_number_class',
        'code_number_school_year',
        'code_number_student',
        'code_number_teacher',
        'training_point'
    ];

    public function students()
    {
        return $this->belongsTo(Student::class, 'code_number_student', 'code_number_student');
    }

    public function classrooms()
    {
        return $this->belongsTo(Classroom::class, 'code_number_class', 'code_number_class');
    }

    public function schoolYears()
    {
        return $this->belongsTo(SchoolYear::class, 'code_number_school_year', 'id');
    }

    public function teachers()
    {
        return $this->belongsTo(Teacher::class, 'code_number_teacher', 'code_number_teacher');
    }
}
