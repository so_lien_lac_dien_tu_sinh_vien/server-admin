<?php

namespace App;

use App\Support\DataTablePaginate;
use Illuminate\Database\Eloquent\Model;

class ClassroomStudent extends Model
{
    use DataTablePaginate;

    protected $table = "classroom_students";

    protected $fillable = [
        'code_number_class',
        'code_number_student',
    ];

    protected $filter = [
        'id',
        'code_number_class',
        'code_number_student',
    ];

    public function students()
    {
        return $this->belongsTo(Student::class, 'code_number_student', 'code_number_student');
    }

    public function classrooms()
    {
        return $this->belongsTo(Classroom::class, 'code_number_class', 'code_number_class');
    }
}
